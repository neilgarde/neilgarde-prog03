# This imports all the layers for "watch-full" into watchFullLayers
watchFullLayers = Framer.Importer.load "imported/watch-full"

# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop


for layerGroupName of watchFullLayers
	window[layerGroupName] = watchFullLayers[layerGroupName]
	
slideup = (layer) ->
	layer.animate
		properties:
			y: -1660
			time: 0
		curve: 'ease'
		
fade = (layer) ->
	layer.animate
		properties:
			opacity: 0
			time: 2
		curve: 'ease'
		
screen1.on	Events.Click, ->
	Utils.delay 0.3, ->
		slideup(screen1)

screen2.on	Events.Click, ->
	screen2.sendToBack()
	
screen3.on Events.Click, ->
	Utils.delay 0.3, ->
		fade(screen3)
