# This imports all the layers for "phone-01" into phone01Layers
phone01Layers = Framer.Importer.load "imported/phone-01"

for layerGroupName of phone01Layers
	window[layerGroupName] = phone01Layers[layerGroupName]

fade = (layer) ->
	layer.animate
		properties:
			opacity: 0
			time: 2
		curve: 'ease'
		
slideleft = (layer) ->
	layer.animate
		properties:
			x: -640
			time: 0
		curve: 'ease'
		
slidecenter = (layer) ->
	layer.animate
		properties:
			x: 0
			time: 0
		curve: 'ease'

slideright = (layer) ->
	layer.animate
		properties:
			x: 855
			time: 0.2
		curve: 'ease'

slideup = (layer) ->
	layer.animate
		properties:
			y: -1660
			time: 0
		curve: 'ease'
		
		
screen1.on	Events.Click, ->
	screen1x2.bringToFront()
	screen1.sendToBack()
	slideright(screen2)
	Utils.delay 0.5, ->
		slideleft(screen1x2)
		slidecenter(screen2)
		
		
screen2.on	Events.Click, ->
	screen2x2.bringToFront()
	screen2.sendToBack()
	slideright(screen3)
	Utils.delay 0.5, ->
		slideleft(screen2x2)
		slidecenter(screen3)
		
		
screen3.on Events.Click, ->
	Utils.delay 0.3, ->
		slideup(screen3)

screen4.on Events.Click, ->
	Utils.delay 0.3, ->
		slideup(screen4)
		
screen5.on Events.Click, ->
	Utils.delay 0.3, ->
		slideup(screen5)