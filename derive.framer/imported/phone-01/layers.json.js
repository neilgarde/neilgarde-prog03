window.__imported__ = window.__imported__ || {};
window.__imported__["phone-01/layers.json.js"] = [
	{
		"id": 19,
		"name": "screen6",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen6.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214118"
	},
	{
		"id": 21,
		"name": "screen5",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen5.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214120"
	},
	{
		"id": 23,
		"name": "screen4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen4.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214123"
	},
	{
		"id": 27,
		"name": "screen3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214149"
	},
	{
		"id": 30,
		"name": "screen2x2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen2x2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "810567409"
	},
	{
		"id": 25,
		"name": "screen2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214151"
	},
	{
		"id": 17,
		"name": "screen1x2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen1x2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "642214152"
	},
	{
		"id": 15,
		"name": "screen1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "850753804"
	}
]