window.__imported__ = window.__imported__ || {};
window.__imported__["watch-full/layers.json.js"] = [
	{
		"id": 13,
		"name": "screen4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen4.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "646533184"
	},
	{
		"id": 7,
		"name": "screen3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1377598920"
	},
	{
		"id": 3,
		"name": "screen2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1641472759"
	},
	{
		"id": 10,
		"name": "screen1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": {
			"path": "images/screen1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 320,
				"height": 400
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2035493979"
	}
]